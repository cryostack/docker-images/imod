FROM ubuntu:18.04

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install wget default-jre-headless python libjpeg62 libglu1-mesa libgtk2.0-0 libidn11 libsm6 x11-apps sudo -y && rm -rf /var/cache/apt/

WORKDIR /opt/src

RUN wget https://bio3d.colorado.edu/imod/AMD64-RHEL5/imod_4.9.12_RHEL7-64_CUDA8.0.sh
RUN sudo sh /opt/src/imod_4.9.12_RHEL7-64_CUDA8.0.sh -y
RUN rm /opt/src/imod_4.9.12_RHEL7-64_CUDA8.0.sh

ENV IMOD_DIR /usr/local/IMOD
ENV IMOD_JAVADIR /usr/local/java
ENV PATH="${PATH}:${IMOD_DIR}/bin"
ENV IMOD_PLUGIN_DIR="$IMOD_DIR/lib/imodplug"
ENV LD_LIBRARY_PATH="$IMOD_DIR/lib:$LD_LIBRARY_PATH"
ENV IMOD_CALIB_DIR="${IMOD_CALIB_DIR}:/usr/local/ImodCalib"
ENV FOR_DISABLE_STACK_TRACE=1
ENV IMOD_QTLIBDIR="$IMOD_DIR/qtlib"

#WORKDIR /usr/local

#COPY ./src/Particle* /usr/local/

#RUN if [ ! -f Particle_1.13.0_linux.tgz ]; then wget http://bio3d.colorado.edu/ftp/PEET/linux/Particle_1.13.0_linux.tgz; fi
#RUN if [ ! -f ParticleRuntimeV95_linux.tgz ]; then wget http://bio3d.colorado.edu/ftp/PEET/linux/ParticleRuntimeV95_linux.tgz; fi

#RUN tar xvfz Particle_1.13.0_linux.tgz
#RUN tar xvfz ParticleRuntimeV95_linux.tgz

#RUN rm Particle_1.13.0_linux.tgz
#RUN rm ParticleRuntimeV95_linux.tgz


#ENV PARTICLE_DIR=/usr/local/Particle
#ENV PATH="$PARTICLE_DIR/bin:$PATH"
#ENV MANPATH="$PARTICLE_DIR/man:$(manpath)"

ENV QT_X11_NO_MITSHM 1

RUN useradd -ms /bin/bash user
USER user

WORKDIR /workdir
